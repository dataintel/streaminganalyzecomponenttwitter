
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-twitteranalyzed',
  template:`<style>/* For the "inset" look only */



html {
    overflow: auto;

}

body {
    position: absolute;
    top: 20px;
    left: 20px;
    bottom: 20px;
    right: 20px;
    padding: 30px; 
    overflow-y: scroll;
    overflow-x: hidden;
    
}

/* Let's get this party started */
::-webkit-scrollbar {
    width: 10px;
}
 
/* Track */
::-webkit-scrollbar-track {
    -webkit-box-shadow: inset 0 0 6px rgba(0,0,0,0.3); 
    -webkit-border-radius: 10px;
    border-radius: 10px;
}
 
/* Handle */
::-webkit-scrollbar-thumb {
    -webkit-border-radius: 10px;
    border-radius: 10px;
    background: slategrey; 
    -webkit-box-shadow: inset 0 0 6px rgba(0,0,0,0.5); 
}
::-webkit-scrollbar-thumb:window-inactive {
	background: gray; 
}

md-list-item{
    margin: 2px 2px 2px 2px;
    margin-left: -13px;
    margin-right: -13px;
}
.row{
    border-bottom: solid 1px;
    margin-bottom: 5px;

}
.image{
   align-content: center;
      display: block;
      margin: 0px auto;
 
    width: 80px;
    height: 100px;
   
}</style> 
<div id="utama" class="col-xs-5" style="max-height:200px;overflow-y:scroll;">
  <div class="row" *ngFor ="let datacarrier of dataLoader">
    <div class="image col-md-2">
      <img src= {{datacarrier.image}} alt="..." class="img-responsive">
    </div>
    <div class="col-md-10">
      <small style="font-size:12px;color: gray;">{{datacarrier.screen_name}} . {{datacarrier.timestamp}}</small>
      <p>
      <span>{{datacarrier.tweet_text}}</span>
      </p>
        <!--<md-list-item></md-list-item>-->
    </div>
          <hr>
    </div>

</div>`
})
export class BaseWidgetStreamingAnalyzeTwComponent implements OnInit {
  public dataLoader: Array<any> = [
	 {
      "screen_name" : "@ernakarina",
      "timestamp" : "1 hours ago",
      "image" : "assets/img/foto1.jpg",
      "tweet_text":"Air adalah senyawa yang penting bagi semua bentuk kehidupan yang diketahui sampai saat ini di Bumi, tetapi tidak di planet lain. Air menutupi hampir 71% permukaan Bumi. Terdapat 1,4 triliun kilometer kubik (330 juta mil³) tersedia di Bumi."
    },
    {
      "screen_name" : "@liviaaniston",
      "timestamp" : "2 hours ago",
      "image" : "assets/img/foto2.jpg",
      "tweet_text":"API (Application Programming Interface) adalah sekumpulan perintah, fungsi, komponen, dan protokol yang disediakan oleh sistem operasi ataupun bahasa pemrograman tertentu yang dapat digunakan oleh programmer saat membangun perangkat lunak"
    },
     {
      "screen_name" : "@medialisar",
      "timestamp" : "3 hours ago",
      "image" : "assets/img/foto3.jpg",
      "tweet_text":"Udara adalah suatu campuran gas yang terdapat pada lapisan yang mengelilingi bumi."
    },
     {
      "screen_name" : "@nadayolanda",
      "timestamp" : "4 hours ago",
      "image" : "assets/img/foto4.jpg",
      "tweet_text":"Air adalah senyawa yang penting bagi semua bentuk kehidupan yang diketahui sampai saat ini di Bumi, tetapi tidak di planet lain. Air menutupi hampir 71% permukaan Bumi. Terdapat 1,4 triliun kilometer kubik (330 juta mil³) tersedia di Bumi."
    },
     {
      "screen_name" : "@indraaries",
      "timestamp" : "5 hours ago",
      "image" : "assets/img/foto5.jpg",
      "tweet_text":"Air adalah senyawa yang penting bagi semua bentuk kehidupan yang diketahui sampai saat ini di Bumi, tetapi tidak di planet lain. Air menutupi hampir 71% permukaan Bumi. Terdapat 1,4 triliun kilometer kubik (330 juta mil³) tersedia di Bumi."
    }, {
      "screen_name" : "@akbarwahyudi",
      "timestamp" : "6 hours ago",
      "image" : "assets/img/foto6.jpg",
      "tweet_text":"Air adalah senyawa yang penting bagi semua bentuk kehidupan yang diketahui sampai saat ini di Bumi, tetapi tidak di planet lain. Air menutupi hampir 71% permukaan Bumi. Terdapat 1,4 triliun kilometer kubik (330 juta mil³) tersedia di Bumi."
    }

    ];

    

  constructor() {console.log(this.dataLoader);}
  
  ngOnInit() { 
  }

}