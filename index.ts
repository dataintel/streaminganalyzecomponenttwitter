import { NgModule, ModuleWithProviders } from '@angular/core';
import { CommonModule } from '@angular/common';

import { BaseWidgetStreamingAnalyzeTwComponent  } from './src/base-widget.component';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {MaterialModule} from '@angular/material';

export * from './src/base-widget.component';

@NgModule({
  imports: [
    CommonModule,
      BrowserAnimationsModule,
    MaterialModule,
  ],
  declarations: [
   
	BaseWidgetStreamingAnalyzeTwComponent 
  ],
  exports: [
	BaseWidgetStreamingAnalyzeTwComponent 
  ]
})
export class TwitterAnalyzedModule {
  static forRoot(): ModuleWithProviders {
    return {
      ngModule: TwitterAnalyzedModule,
      providers: []
    };
  }
}
